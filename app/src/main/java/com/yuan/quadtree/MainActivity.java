package com.yuan.quadtree;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    private Spinner startNodeSpinner;
    private List<Rule> ruleList = new ArrayList<>();
    private Map<String, Node> nodeMap = new HashMap();

    class Node {
        public String mName;
        public List<String> mListNodeName;
        public int curNum;
        public boolean visted;

        public Node(String name, String[] nodes) {
            this.mListNodeName = new ArrayList<>();
            for (String node : nodes)
                mListNodeName.add(node);
            this.mName = name;
            this.curNum = 0;
            this.visted = false;
        }
    }

    class Rule {
        public List<String> mListNode;
        public int mSum;

        public Rule(String[] strings, int sum) {
            this.mListNode = new ArrayList<>();
            for (String s : strings)
                mListNode.add(s);
            this.mSum = sum;
        }

        public boolean check(List<String> stringList, List<Integer> nums) {
            boolean b = true;
            int containNum = 0;
            int curSum = 0;
            for (int i = 0; i < stringList.size(); i++) {
                if (mListNode.contains(stringList.get(i))) {
                    curSum += nums.get(i);
                    containNum++;
                }
            }
            if (containNum == 3) {
                //有三個點時檢查是否相等
                if (curSum != mSum)
                    b = false;
            } else if (containNum == 2) {
                //有兩個點時檢查是否過小或是超過
                if ((curSum + 10) < mSum)
                    b = false;
                if (curSum > mSum)
                    b = false;
            }
            return b;
        }
    }

    class Solution {
        List<Node> solutionList;
        int nodeCount;

        public Solution(List<Node> solutionList, int nodeCount) {
            this.solutionList = solutionList;
            this.nodeCount = nodeCount;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startNodeSpinner = findViewById(R.id.spinner);

        setRuleList();

        findViewById(R.id.buttonBFS).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNodeMap();
                List<Node> nodeListBFS = findRouteByBFS(startNodeSpinner.getSelectedItem().toString());
                Solution solution = findSolution(nodeListBFS);
                print("bfs", solution);
            }
        });
        findViewById(R.id.buttonDFS).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNodeMap();
                List<Node> nodeListDFS = findRouteByDFS(startNodeSpinner.getSelectedItem().toString());
                Solution solution = findSolution(nodeListDFS);
                print("dfs", solution);
            }
        });

        findViewById(R.id.buttonFastAI).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(List<String> pl : Permutation.perm(Arrays.asList("b", "c", "d", "e", "f", "g", "i", "j"))) {
                    setNodeMap();
                    pl.add(0, "a");
                    pl.add("h");
                    Solution solution = findSolutionUnderCountNum(pl, 406);
                    if(solution != null ) {
                        print("fast", solution);
                    }
                }
            }
        });
    }

    private void setRuleList() {
        ruleList.clear();
        ruleList.add(new Rule(new String[]{"a", "c", "d"}, 19));
        ruleList.add(new Rule(new String[]{"a", "d", "e"}, 18));
        ruleList.add(new Rule(new String[]{"d", "e", "g"}, 16));
        ruleList.add(new Rule(new String[]{"g", "h", "j"}, 22));
        ruleList.add(new Rule(new String[]{"f", "h", "i"}, 17));
        ruleList.add(new Rule(new String[]{"b", "c", "f"}, 11));
    }

    private void setNodeMap() {
        nodeMap.clear();
        nodeMap.put("a", new Node("a", new String[]{"b", "c", "d", "e"}));
        nodeMap.put("b", new Node("b", new String[]{"i", "f", "c", "a"}));
        nodeMap.put("c", new Node("c", new String[]{"f", "d", "a", "b"}));
        nodeMap.put("d", new Node("d", new String[]{"g", "e", "a", "c"}));
        nodeMap.put("e", new Node("e", new String[]{"j", "a", "d", "g"}));
        nodeMap.put("f", new Node("f", new String[]{"i", "h", "c", "b"}));
        nodeMap.put("g", new Node("g", new String[]{"j", "e", "d", "h"}));
        nodeMap.put("h", new Node("h", new String[]{"j", "g", "f", "i"}));
        nodeMap.put("i", new Node("i", new String[]{"j", "h", "f", "b"}));
        nodeMap.put("j", new Node("j", new String[]{"e", "g", "h", "i"}));
    }

    private List<Node> findRouteByBFS(String startNodeName) {
        List<Node> nodeList = new ArrayList<>();
        Queue<Node> nodeQueue = new LinkedList<>();
        //startNode
        Node startNode = nodeMap.get(startNodeName);
        startNode.visted = true;
        nodeQueue.add(startNode);
        while (nodeQueue.size() > 0) {
            Node curNode = nodeQueue.poll();
            //將node從queue拿出來放在結果的List中
            nodeList.add(curNode);
            for (String nodeString : curNode.mListNodeName) {
                Node node = nodeMap.get(nodeString);
                if (node.visted == false) {
                    node.visted = true;
                    nodeQueue.add(node);
                }
            }
        }
        return nodeList;
    }

    private List<Node> findRouteByDFS(String startNodeName) {
        List<Node> nodeList = new ArrayList<>();
        Stack<Node> nodeStack = new Stack<>();

        //startNode
        Node startNode = nodeMap.get(startNodeName);
        startNode.visted = true;
        nodeStack.push(startNode);
        while (nodeStack.size() > 0) {
            Node curNode = nodeStack.pop();
            nodeList.add(curNode);
            for (int i = curNode.mListNodeName.size() -1 ; i >= 0 ; i--) {
                Node node = nodeMap.get(curNode.mListNodeName.get(i));
                if (node.visted == false) {
                    node.visted = true;
                    nodeStack.push(node);
                }
            }
        }
        return nodeList;
    }

    private Solution findSolution(List<Node> nodeList) {
        int count = 0;
        List<Node> solutionList = new ArrayList<>();
        while (solutionList.size() != 10 || !checkNodeList(solutionList)) {
            count++;
            if (solutionList.size() == 0)
                solutionList.add(nodeList.get(0));
            Node node = solutionList.get(solutionList.size() - 1);
            if (node.curNum == 10) {
                node.curNum = 0;
                solutionList.remove(solutionList.size() - 1);
                continue;
            }
            node.curNum++;
            //檢查解
            if (!checkNodeList(solutionList)) {
                //最上層節點未到10，繼續加，或是已找到解
                continue;
            } else {
                if (solutionList.size() < nodeList.size()) {
                    //有下一個節點
                    solutionList.add(nodeList.get(solutionList.size()));
                }
            }
        }
        return new Solution(solutionList, count);
    }

    private Solution findSolutionUnderCountNum(List<String> nodeList, int countNum) {
        int count = 0;
        List<Node> solutionList = new ArrayList<>();
        while (solutionList.size() != 10 || !checkNodeList(solutionList)) {
            count++;
            if(count > countNum) {
                return null;
            }
            if (solutionList.size() == 0)
                solutionList.add(nodeMap.get(nodeList.get(0)));
            Node node = solutionList.get(solutionList.size() - 1);
            if (node.curNum == 10) {
                node.curNum = 0;
                solutionList.remove(solutionList.size() - 1);
                continue;
            }
            node.curNum++;
            //檢查解
            if (!checkNodeList(solutionList)) {
                //最上層節點未到10，繼續加，或是已找到解
                continue;
            } else {
                if (solutionList.size() < nodeList.size()) {
                    //有下一個節點
                    solutionList.add(nodeMap.get(nodeList.get(solutionList.size())));
                }
            }
        }
        if(solutionList.size() == 10) {
            return new Solution(solutionList, count);
        } else {
            return null;
        }
    }

    private boolean checkNodeList(List<Node> nodeList) {
        boolean b = true;
        for (int i = 0; i < ruleList.size(); i++) {
            Rule rule = ruleList.get(i);
            List<String> stringList = new ArrayList<>();
            List<Integer> integerList = new ArrayList<>();
            for (int j = 0; j < rule.mListNode.size(); j++) {
                Node node = nodeMap.get(rule.mListNode.get(j));
                if (node.curNum > 0) {
                    stringList.add(node.mName);
                    integerList.add(node.curNum);
                    if (!checkAllRule(stringList, integerList)) {
                        b = false;
                        break;
                    }
                }
            }
        }
        if (nodeList.size() == 10) {
            for (Node node : nodeList) {
                if (node.curNum == 0)
                    b = false;
            }
        }
        return b;
    }

    private boolean checkAllRule(List<String> strings, List<Integer> nums) {
        boolean b = true;
        for (Rule rule : ruleList) {
            if (!rule.check(strings, nums)) {
                b = false;
                break;
            }
        }
        return b;
    }

    private void print(String type, Solution solution) {
        for (Node node : solution.solutionList) {
            switch (node.mName) {
                case "a":
                    ((TextView) findViewById(R.id.textViewA)).setText("" + node.curNum);
                    break;
                case "b":
                    ((TextView) findViewById(R.id.textViewB)).setText("" + node.curNum);
                    break;
                case "c":
                    ((TextView) findViewById(R.id.textViewC)).setText("" + node.curNum);
                    break;
                case "d":
                    ((TextView) findViewById(R.id.textViewD)).setText("" + node.curNum);
                    break;
                case "e":
                    ((TextView) findViewById(R.id.textViewE)).setText("" + node.curNum);
                    break;
                case "f":
                    ((TextView) findViewById(R.id.textViewF)).setText("" + node.curNum);
                    break;
                case "g":
                    ((TextView) findViewById(R.id.textViewG)).setText("" + node.curNum);
                    break;
                case "h":
                    ((TextView) findViewById(R.id.textViewH)).setText("" + node.curNum);
                    break;
                case "i":
                    ((TextView) findViewById(R.id.textViewI)).setText("" + node.curNum);
                    break;
                case "j":
                    ((TextView) findViewById(R.id.textViewJ)).setText("" + node.curNum);
                    break;
            }
        }
        String route = "";
        for (Node node : solution.solutionList) {
            route += node.mName + "->";
        }
        if (type.equalsIgnoreCase("bfs")) {
            ((TextView) findViewById(R.id.textViewBFSRoute)).setText("" + route);
            ((TextView) findViewById(R.id.textViewNodeBFS)).setText("" + solution.nodeCount);
        } else if (type.equalsIgnoreCase("dfs")) {
            ((TextView) findViewById(R.id.textViewDFSRoute)).setText("" + route);
            ((TextView) findViewById(R.id.textViewNodeDFS)).setText("" + solution.nodeCount);
        } else if (type.equalsIgnoreCase("fast")) {
            ((TextView) findViewById(R.id.textViewFastRoute)).setText("" + route);
            ((TextView) findViewById(R.id.textViewNodeFast)).setText("" + solution.nodeCount);
        }
    }
}
